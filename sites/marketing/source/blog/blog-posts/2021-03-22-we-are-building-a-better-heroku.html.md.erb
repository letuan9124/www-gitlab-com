---
title: "We are building a better Heroku"
author: Michael Friedrich
author_gitlab: dnsmichi
author_twitter: dnsmichi
categories: unfiltered
image_title: '/images/blogimages/better-heroku-5min-prod-app/spacex-unsplash.jpg'
description: "Ended up in a Heroku blackbox for your stateful web app? GitLab introduces a better Heroku integrated into your DevSecOps workflow: The 5 minute production app."
tags: cloud native, DevOps, CD
#cta_button_text:
#cta_button_link:
guest: false
ee_cta: false
install_cta: false
twitter_text: ".@gitlab is building a better Heroku: The 5 minute production app #SeeingIsBelieving"
postType: corporate, dev-evangelism
merch_banner: merch_one
merch_sidebar: merch_one
related_posts:
  - "/blog/2020/12/15/first-code-to-ci-cd-deployments-in-5-minutes/"
  - "/blog/2021/02/24/production-grade-infra-devsecops-with-five-minute-production/"
  - "/blog/2020/12/15/deploy-aws/"
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

Creating a web application has become very convenient and easy. You’ll start in your local development environment, run a dev server and verify the changes looking good. At a certain point, you want to share it with your friends on the internet. A service or server?

### Use Heroku

I have been a backend focussed developer in the past 20 years, web development is often fighting with Javascript and CSS. Especially Heroku as a deployment platform is a new area for me.

Let's start with creating an account, login, and follow the web instructions to create a new app.

![First steps with Heroku](/images/blogimages/better-heroku-5min-prod-app/heroku_first_app_fail.png){: .shadow.medium.center}

Ok. After some trial and error, the name needs to be unique in the global namespace. Time to stop and read the [documentation](https://devcenter.heroku.com/).

#### Reboot Heroku

Let’s try a fun demo, a battleship game to learn Javascript on the client and NodeJS on the server.

```
$ cd ~/dev/opensource
$ git clone https://github.com/kubowania/battleships
$ cd battleships
```

Test it locally, optional.

```
$ npm install
$ npm start
```

Install the Heroku CLI, on [macOS with Homebrew](/blog/2020/04/17/dotfiles-document-and-automate-your-macbook-setup/).

```
$ brew install heroku/brew/heroku

$ heroku autocomplete
```

This opens a new browser window to login. Lets create an app.

```
$ heroku create
Creating app... done, ⬢ nameless-mountain-48655
https://nameless-mountain-48655.herokuapp.com/ | https://git.heroku.com/nameless-mountain-48655.git
```

The Git URL unfortunately does not provide access.

![No Git web access in Heroku](/images/blogimages/better-heroku-5min-prod-app/heroku_git_not_allowed.png){: .shadow.medium.center}

The CLI command adds a new Git remote called `heroku` where we need to push into.

```
$ git push heroku main

remote: -----> Launching...
remote:        Released v3
remote:        https://nameless-mountain-48655.herokuapp.com/ deployed to Heroku
remote:
remote: Verifying deploy... done.
```

Deployed in less than 5 minutes. Getting there and installing the pre-requisites on the CLI took longer than expected.

![Battleship web app deployed with Heroku](/images/blogimages/better-heroku-5min-prod-app/battleship_heroku.png){: .shadow.medium.center}

Lots of CLI commands involved, and it did not run in a CI/CD pipeline with additional tests before deploying it. Now the web application is deployed into a black box. Want to use Let’s Encrypt and your own domain name? How about adding the deployment natively to GitLab to have a single application in your DevOps workflow?

#### Do not use Heroku

{::options parse_block_html="false" /}

<div class="center">

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Heroku is like a fallen civilization of elves. Beautiful, immortal, beloved by all who encountered it - but still a dead end.</p>&mdash; Adam Jacob (@adamhjk) <a href="https://twitter.com/adamhjk/status/1369704730218299392?ref_src=twsrc%5Etfw">March 10, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>

Apparently Heroku is not Open Source, making contributions not so easy. You cannot debug Heroku by reading the source code.

### A better Heroku: The 5 minute production app

{::options parse_block_html="false" /}

<div class="center">

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">the modern tech industry is basically folks just endlessly remaking remakes of heroku</p>&mdash; Always Miso (@monkchips) <a href="https://twitter.com/monkchips/status/1368924845740810249?ref_src=twsrc%5Etfw">March 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Truth <a href="https://t.co/AFN9anBbQG">https://t.co/AFN9anBbQG</a></p>&mdash; Sid Sijbrandij (@sytses) <a href="https://twitter.com/sytses/status/1368982067229253632?ref_src=twsrc%5Etfw">March 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>


Cloud resources are cheap. AWS offers a free tier, HashiCorp Terraform has become an excellent tool to manage multi-cloud resources and GitLab integrates app packaging, container registry, deployment and TLS certificates.

There’s more application goodies: Provision a PostgreSQL VM, add Redis, SMTP email transport, custom domains with Let’s Encrypt.

#### Use the 5 minute production app

The [documentation](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template#usage) says to create a new AWS IAM role with credentials for automation.

The second step is to have the source code available in a GitLab project. You can use `New project > Import project > Repo by URL` to automatically import the GitHub repository `https://github.com/kubowania/battleships.git`.

![Import the GitHub repository into GitLab](/images/blogimages/better-heroku-5min-prod-app/gitlab_new_project_import_github_url.png){: .shadow.medium.center}

Once imported, navigate into `Settings > CI/CD > Variables` to specify the AWS credentials and region. Ensure to tick the `Masked` checkbox to hide them in all job logs.

![Configure AWS credentials as masked CI/CD variables](/images/blogimages/better-heroku-5min-prod-app/gitlab_5minprodapp_aws_cicd_variables.png){: .shadow.medium.center}

Navigate back into the project overview. Click the `Setup CI/CD` button or open the Web IDE to create a new `.gitlab-ci.yml` file. Add the remote CI/CD template include like this:

```
variables:
    TF_VAR_DISABLE_POSTGRES: "true"
    TF_VAR_DISABLE_REDIS: "true"

include:
  remote: https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/raw/stable/deploy.yml
```

The battleship application does not need the PostgreSQL and Redis backends. They are disabled with setting `TF_VAR_DISABLE_POSTGRES` and `TF_VAR_DISABLE_REDIS` [variables](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/blob/master/VARIABLES.md) to `false`.

Commit the change to the default branch.

8:43pm CET: Pipeline started with the build job. 2 min 33 sec.

![GitLab pipeline builds the Docker image with Auto-Build](/images/blogimages/better-heroku-5min-prod-app/gitlab_5minprodapp_pipeline_01.png){: .shadow.medium.center}

8:45pm CET: Pipeline runs terraform_apply to provision AWS resources in 2min 47 sec.

![GitLab pipeline runs Terraform to provision cloud resources in AWS](/images/blogimages/better-heroku-5min-prod-app/gitlab_5minprodapp_pipeline_02.png){: .shadow.medium.center}

8:48pm CET: Deployed in 1 min 11 sec.

The deploy job log greets with the URL in ~5 minutes, including a Lets Encrypt TLS certificate. There we go, let’s play some battleship!

![Battleship web app deployed in AWS with the 5 minute production app](/images/blogimages/better-heroku-5min-prod-app/battleship_5minprodapp_aws.png){: .shadow.medium.center}

Note that we never left the browser, there is no CLI involved. Next to the included template, there’s also room for adding more CI tests and security best practices while hacking on this project. You can navigate into your AWS console for debugging and troubleshooting, and plan with production budgets where needed.

### 5 minute production app + DevSecOps = ❤️

Example for [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) and [SAST](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html):

```
include:
  - remote: https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/raw/stable/deploy.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
```

### More to use: Database backends, TLS, environments

This blog post covers the basic learning steps with Heroku and the 5 minute production app. A typical web app requires a database, storage or caching backend, which can get complicated to run with Heroku. We will explore the setup and production experience in future blog posts. In addition to backends, we will also look into TLS certificates and production environments in CD workflows.

Meanwhile, try the 5 min production app yourself:

* [5 minute production app docs](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template#the-5-minute-production-app)
* [Example projects](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template#examples)
* Your own future web app with [your custom domain](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template#custom-domain)?

Cover image by [SpaceX](https://unsplash.com/@spacex) on [Unsplash](https://unsplash.com/photos/OHOU-5UVIYQ)

