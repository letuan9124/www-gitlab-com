---
layout: markdown_page
title: "Contributing to Engagement"
description: "Help us foster a fun and inclusive community while reaching new users and contributors."
canonical_path: "/community/contribute/engagement/"
---

## Engagement

### Organize a Meetup or Event
1. Follow the instructions here to [organize a GitLab meetup](/community/meetups/)
1. If you're looking for funding, please follow the instrucions here on how to [request funding for your GitLab-related event](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/new?issuable_template=sponsorship-request). Here's more about [how we make a decision to sponsor an event](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/#community-events). We also have a [Diversity, Inclusion, and Belonging (DIB) Scholarship](https://about.gitlab.com/community/sponsorship/) to help those who are organizing DIB-related events.
1. We have resources in case you are [looking for speakers for your event](/handbook/marketing/community-relations/evangelist-program/#find-a-tech-speaker).
1. Once it's planned and scheduled, consider listing your event on our [Upcoming events page](https://about.gitlab.com/events/) and promoting using the `GitLab` hashtag. 
1. Remember to keep in mind [GitLab's tips on creating an inclusive event](/handbook/marketing/community-relations/#-community-diversity-inclusion-and-belonging)!
1. Consider asking people who attend your meetup or event to sign up for [GitLab's First Look](/community/gitlab-first-look/) program to help us continue to design the best DevOps platform out there.

### Speak at an event
1. Sign up for events and give a talk about GitLab. Everyone can contribute! Whether you're speaking about how people can join the GitLab community, providing tips and tricks for how to use GitLab, or giving a technical deep dive, all of that is equally valuable and helps us reach more potential users and contributors. 
1. When asked to sign up, make sure you list yourself as a 'GitLab Community Member` so as not to represent yourself as a paid employee or contractor, if you are not one.
1. Share which events you'll be participating in on our [Community Forum](https://forum.gitlab.com/) (in the `Community` section, so that others can participate as well!
1. Consider adding yourself as part of the [GitLab Speaker's Burueau](https://about.gitlab.com/speakers/) to help others find you. 

### Write a blog post
1. Consider writing a post about your experience using GitLab, about how to join the community, or anything else GitLab related. Wherever possible, point people back to the GitLab website to help us get additional visibility. 

### Contribute to our community experience
1. Add ideas for how we can continue to grow the GitLab community on the [GitLab Community Forum](https://forum.gitlab.com/)
