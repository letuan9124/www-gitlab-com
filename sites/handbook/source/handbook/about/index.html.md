---
layout: handbook-page-toc
title: "About the Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## History of the handbook

The handbook started when GitLab was a company of just ten people to make sharing information efficient and easy.
We knew that future GitLab team-members wouldn't be able to see emails about process changes that were being sent before they joined and that most of the people who would eventually join GitLab likely hadn't even heard of us yet.
The handbook was our way of ensuring that all of our company information was accessible to everyone regardless of when they became part of the team.

## Advantages

At GitLab our handbook is extensive and keeping it relevant is an important part of everyone's job.
It is a vital part of who we are and how we communicate.
We established these processes because we saw these benefits:

1. Reading is much faster than listening.
1. Reading is async, you don't have to interrupt someone or wait for them to become available.
1. Recruiting is easier if people can see what we stand for and how we operate.
1. Retention is better if people know what they are getting into before they join.
1. On-boarding is easier if you can find all relevant information spelled out.
1. Teamwork is easier if you can read how other parts of the company work.
1. Discussing changes is easier if you can read what the current process is.
1. Communicating change is easier if you can just point to the diff.
1. Everyone can contribute to it by proposing a change via a merge request.

One common concern newcomers to the handbook express is that the strict documentation makes the company more rigid.
In fact, writing down our current process in the handbook has the effect of empowering contributors to propose change.
As a result, this handbook is far from rigid. You only need to look at the [handbook changelog](/handbook/CHANGELOG.html) to see the evidence.  Every attempt is made to document guidelines and processes in the handbook. However, it is not possible to document every possible situation or scenario that could potentially occur.  Just because something is not yet in the handbook does not mean that it is allowed.  GitLab will review each team member's concern or situation based on local laws to determine the best outcome and then update the handbook accordingly. If you have questions, please discuss with your manager or contact the [People Success](/handbook/people-group/) team.

## Handbook Interpretation

The handbook is subject to interpretation.  We do our best to be as clear as possible to minimize confusion and/or misinterpretation.  We also recognize that we have a global audience and that may bring different interpretations.  If you have any questions or need further clarification please check with the content owner of the page.  When in doubt please reach out and ask.

**Remember that [everything is in draft](https://about.gitlab.com/handbook/values/#everything-is-in-draft) at GitLab and subject to change, this includes our handbook.**


## Count handbook pages

It's easy to see that the handbook is large, but have you ever wondered just _how_ large?
If it were printed in 12-point, single-spaced Arial it would be well over ten _thousand_ pages long and that's not even counting the images.
That's a lot of good info!

### Historical Word and Page Counts

| **Date**   | **Words** | **Pages** |
| ---------- | --------- | --------- |
| 2018-11-01 | 901,267   | 2,002     |
| 2019-05-28 | 1,331,972 | 2,960     |
| 2019-07-15 | 1,429,287 | 3,176     |
| 2019-09-30 | 1,667,320 | 3,705     |
| 2020-03-17 | 2,422,131 | 5,383     |
| 2020-06-22 | 3,217,930 | 7,151     |
| 2020-09-22 | 3,813,044 | 8,474     |
| 2021-01-12 | 4,713,795 | 10,476    |

### Methodology

Page counts are determined through a simple two-step process:

1. Count the number of words in the handbook. This can be done by running `find sites/handbook/source/handbook -type f | xargs wc -w` from the root of the repository.
1. The sum of the two commands are the total number of words in the handbook.
1. Submit the word count to [WordCounter](https://wordcounter.net/words-per-page) for conversion to a page count.

## More about the handbook

We've gathered _some_ information about the handbook here, but there's still more elsewhere.

- [Handbook usage](/handbook/handbook-usage/)
- [Evolution of the handbook](/handbook/ceo/#evolution-of-the-handbook) on the [CEO page](/handbook/ceo/)
- [Changelog](/handbook/CHANGELOG.html)
- [Handbook editing examples](/handbook/practical-handbook-edits/)
