---
layout: handbook-page-toc
title: "People Compliance Audits"
description: "The People Compliance team at GitLab is the DRI for monthly and quarterly audits related to the team member life cycle."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## People Compliance Audits

The People Compliance team is the point of contact for Legal, Security Compliance and Internal audit-related queries that include any type of personal team member information or documents.

## Monthly Audits

### Onboarding Issues 

- Completion and closure of all onboarding issues within 30 days of start date

### Onboarding Requirements by Jurisdiction

- I-9 completion for all US-based team members
- I-9 accuracy I-9 for all US-based team members
- I-9 documentation saved correctly in BambooHR
- I-9 data retention compliance in I-9 system and BambooHR
- Eligibility of work confirmed for all team members
- Proof of eligibility of work documented correctly in BambooHR
- Accuracy of team page due to onboarding
- Completion of PIAA for all team members
- Documentation of correct approvals if team members request exclusions to PIAA 

### Offboarding Issues

- Completion and closure of all offboarding issues within 30 days of offboarding date
- Signature, documentation and retention of all offboarding agreements 
- Accuracy of offboarding information properly logged in HRIS
- Accuracy of team page due to offboarding

### Career Mobility Issues

### Referral Bonuses

## Quarterly Audits

### Country Conversion

Documentation added/changed during a Country Conversion, including: mutual termination agreements, accurate probation period notifications, accurate leave accruals.

## Ongoing and Ad-Hoc Audits

Ongoing audits and compliance initiatives in Total Rewards, People Operations, and Recruiting to include offer approvals, I9 compliance, onboarding, offboarding, various systems, information transfer.

