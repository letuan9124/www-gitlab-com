---
layout: handbook-page-toc
title: "GitLab Special Programs: EDU & OSS"
category: License and subscription
description: Redirecting EDU or OSS subscription inquiries.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

Use this workflow when there's a ticket for either the Education or Open Source program.

---
##### Workflows
1. [GitLab Education program](https://about.gitlab.com/solutions/education/): use the [EDU response macro in ZD](https://gitlab.zendesk.com/agent/admin/macros/360026025159).
1. [GitLab Open Source Software program aka OSS](https://about.gitlab.com/solutions/open-source/join/): use the [OSS response macro in ZD](https://gitlab.zendesk.com/agent/admin/macros/360026025179).
1. We do not offer any non-profit discounts or programs.
1. Internal escalations can be made via Slack channel [#education-oss](https://gitlab.slack.com/archives/CB21NTDJQ)
