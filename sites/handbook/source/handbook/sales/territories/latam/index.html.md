---
layout: handbook-page-toc
title: "LATAM Region Handbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##  LATAM Region Mission

**Work in progress**

## Who are we?

| Team Member Name | Role | Segment | Territory Name | Countries |
| ---------------- | ---- | ------- | -------------- | ----------|
| Carlos Dominguez (EN/ES) | Strategic Account Leader | Enterprise | LATAM North | Venezuela,Turks and Caicos Islands, Trinidad and Tobago, Saint Vincent and the Grenadines, Saint Martin, Saint Lucia, Saint Kitts and Nevis, Puerto Rico, Peru, Panama, Nicaragua, Montserrat, Mexico. Martinique, Jamaica, Honduras, Haiti, Guyana, Guadeloupe, Grenada, French Guiana, Ecuador, Colombia |
| Jim Torres (EN/ES/PT) | Strategic Account Leader | Enterprise | LATAM South | Uruguay, Suriname. Paraguay ,Guatemala, Falkland Islands, El Salvador, Dominican Republic, Dominica, Curacao, Cuba ,Costa Rica, Chile, Cayman Islands, British Virgin Islands, Brazil, Bolivia, Bermuda, Belize, Barbados, Bahamas, Aruba, Argentina, Antigua and Barbuda, Anguilla. |
| Romer Gonzalez (EN/ES/PT) | Account Executive | Mid-Market and SMB | LATAM | All |
| Bruno Lazzarin (EN/ES/PT) | SDR | Mid-Market and SMB | LATAM | All |
| Leo Vieira (EN/ES/PT) | SDR | Enterprise | LATAM | All |
| Hugo Azevedo (EN/ES/PT) | Solution Architect | Enterprise | LATAM | All |
| Ricardo Amarilla (EN/ES/PT) | Technical Account Manager | Enterprise | LATAM | All |

## Technical Support

Please, visit our [Technical Support Handbook Page](https://about.gitlab.com/support/) for detailed and up-to-date information.

We currently offer support for a [few languages](https://about.gitlab.com/support/#language-support) other than English (EN) via support ticket. This includes Spanish (ES) and Portuguese (PT). Should you be offered a call, only English is available.

English is the offical language used on GitLab's [documentation](https://docs.gitlab.com/) and in the [Community Forum](https://forum.gitlab.com/).

## Events, webinars and resources in Spanish and Portuguese.

GitLab LATAM has been promoting several Webinars and events for the entire Spanish and Portuguese speaking community. Below you can find our latest webinars and Events:

* [Webinars in Spanish](https://learn.gitlab.com/c/youtube-2?x=bzkKkv)
* [Webinars in Portuguese](https://learn.gitlab.com/c/youtube-12?x=NsYXMM)

You can also be part of our community through our Meetup groups. There you can be part of our large community in Latin America and follow our upcoming events.

* [México GitLab Meetup](https://www.meetup.com/es/Mexico-City-GitLab-Meetup/)
* [Chile GitLab Meetup](https://www.meetup.com/es/Santiago-GitLab-Meetup/)
* [Brazil GitLab Meetup](https://www.meetup.com/es/Sao-Paulo-GitLab-Meetup/)
* [Paraguay GitLab Meetup](https://www.meetup.com/es/Paraguay-GitLab-Meetup/)
* [Peru GitLab Meetup](https://www.meetup.com/es/GitLab-Peru/)
* [Colombia GitLab Meetup](https://www.meetup.com/es/GitLab-Colombia/)
